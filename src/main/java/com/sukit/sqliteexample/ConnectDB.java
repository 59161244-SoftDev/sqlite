/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.sqliteexample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xenon
 */
public class ConnectDB {
    public static void main(String[] args) {
        Connection conn = null;
        
        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
        } catch (ClassNotFoundException ex) {
            System.out.println("Library org.sqlit not found");
            System.exit(0);
        } catch (SQLException ex) {
            System.out.println("Unable to connect database");
            System.exit(0);
        }
        System.out.println("Connected database Sucessfully");
    }
}
