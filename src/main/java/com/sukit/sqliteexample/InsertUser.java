/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukit.sqliteexample;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xenon
 */
public class InsertUser {

    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO COMPAMNY (ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(1, 'Paul', '32', 'Californoa', 20000);";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO COMPAMNY (ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(2, 'Mark', '29', 'Florida', 25000);";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO COMPAMNY (ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(3, 'Ace', '25', 'Texas', 19800);";
            stmt.executeUpdate(sql);
            
            sql = "INSERT INTO COMPAMNY (ID, NAME, AGE, ADDRESS, SALARY)"
                    + "VALUES(4, 'Bill', '38', 'Los Angelis', 27500);";
            stmt.executeUpdate(sql);
            conn.commit();
            stmt.close();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
